Tactics
============================

A widget collection for Attunement.

To use, include all of Attunement, `widget-base.lua`, and the widget files you'd like to use in your addon in your `.ainf` file's `FileList` section.

Issues can be caused by load order, for best results ensure the following load order.
1. Attunement Files
2. `widget-base.lua`
3. Widgets
4. Your code

Some widgets rely on other widgets - this will be outlined in the documentation for that widget.

Component List and Documentation
-----------------------

* [Circle Segment](docs/circle-segment-widget.md)
* [Circle Progress](docs/circle-progress-widget.md)