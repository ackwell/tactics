
-- Ensure Tactics is defined
if not Tactics then
	Tactics = {}
end

Tactics.WidgetBase = {}
function Tactics.WidgetBase:New()
	local self = {}
	local protected = {}

	-- Private variables
	local attrReadHooks = {}
	local attrWriteHooks = {}
	local mt = {}

	-- Protected functions
	function protected:AddAttrWriteHook(attr, func)
		attrWriteHooks[attr] = func
	end
	function protected:AddAttrReadHook(attr, func)
		attrReadHooks[attr] = func
	end

	-- Proxy (hook) self
	-- Call registered function on matching key
	-- Can only have one function per key
	mt.__newindex = function(table, key, value)
		local temp = attrWriteHooks[key]
		if temp then
			temp(value)
		end
	end
	mt.__index = function(table, key)
		local temp = attrReadHooks[key]
		if temp then
			return temp()
		end
	end
	setmetatable(self, mt)

	-- Return new class
	return self, protected
end

