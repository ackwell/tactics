
if not Tactics then
	Tactics = {}
end

-- Requires circle-segment-widget.lua
-- Uses mutiple CircleSegment's to construct
-- a circular arc or pie based progress bar
-- that is comtrolle by simply assigning 0-100 %
-- to the Percent attribute.

Tactics.CircleProgress = {}
function Tactics.CircleProgress:Bless(self, protected, params)
	-- Arg checking
	assert(params ~= nil, "Params are required")
	assert(params.radius ~= nil, "Must provide radius")
	assert(type(params.radius)=="number", "Radius must be a number")
	assert(params.radius >= 0, "Radius must be at least 0")
	assert(params.radius%1==0, "Radius must be a whole number")
	assert(params.thickness ~= nil, "Must provide thickness")
	assert(type(params.thickness) == "number", "Thickness must be a number")
	assert(params.thickness >= 1, "Thickness must be at least 1")
	assert(params.thickness%1==0, "Thickness must be a whole number")
	assert(params.degrees ~= nil, "Must provide degrees")
	assert(type(params.degrees) == "number", "degrees must be a number")
	assert(params.degrees >= 0 and params.degrees <= 360, "degress must be between 0 and 360")

	-- Private variables and functions
	local percent = 0
	local x = 0
	local y = 0
	local color = "#FFFFFFFF"
	local scale = 1
	local hidden = true
	local rotationDegrees=0
	-- There is a percision issue of some kind it looks like when
	-- dealing with circles. When they are made small (100 pixles or so)
	-- arcs of different lengths don't have the same pixel overlap
	-- so as we switch between them, it looks like the arcs giggle.
	-- To work around this, we generate everything many times the needed size
	-- then back scal it down to what was requested. This is done silently
	-- without the user of this class knowing.
	local overScale = 10000
	local layer = params.Layer or "Dialog"
	local clockwise = params.clockwise or false
	local arcs = {}
	for p=0,100 do
		arcs[p] = Tactics.CircleSegment:New{
			Layer=layer,
			radius=params.radius*overScale,
			thickness=params.thickness*overScale,
			startDegrees=clockwise and (params.degrees*(-p/100)) or 0,
			endDegrees=clockwise and 0 or (params.degrees*(p/100)),
			Hidden=true
		}
	end
	local activeArc = arcs[0]
	local updateActiveArc = function()
		activeArc.X = x
		activeArc.Y = y
		activeArc.Color = color
		activeArc.Scale = scale*(1/overScale)
		activeArc.Hidden = hidden
		activeArc.RotationDegrees=rotationDegrees
	end

	-- Public interfaces
	protected:AddAttrWriteHook("X",function(v)
		x = v
		updateActiveArc()
	end)
	protected:AddAttrReadHook("X",function()
		return x
	end)
	protected:AddAttrWriteHook("Y",function(v)
		y = v
		updateActiveArc()
	end)
	protected:AddAttrReadHook("Y",function()
		return y
	end)
	protected:AddAttrWriteHook("Color",function(v)
		color = v
		updateActiveArc()
	end)
	protected:AddAttrReadHook("Color",function()
		return color
	end)
	protected:AddAttrWriteHook("Scale",function(v)
		scale = v
		updateActiveArc()
	end)
	protected:AddAttrReadHook("Scale",function()
		return scale
	end)
	protected:AddAttrWriteHook("Hidden",function(v)
		hidden = v
		updateActiveArc()
	end)
	protected:AddAttrReadHook("Hidden",function()
		return hidden
	end)
	protected:AddAttrWriteHook("Percent",function(v)
		if(v >= 0 and v <=100) then
			percent = v
			activeArc.Hidden = true
			activeArc = arcs[math.ceil(percent)]
			updateActiveArc()
		else
			error("Percent "..tostring(v).." must be >= 0 and <= 100", 3)
		end
	end)
	protected:AddAttrReadHook("Percent",function()
		return percent
	end)
	protected:AddAttrWriteHook("RotationDegrees",function(v)
		rotationDegrees=v
		updateActiveArc()
	end)
	protected:AddAttrReadHook("RotationDegrees",function()
		return rotationDegrees
	end)
	rawset(self, "Show", function(_)
		hidden = false
		updateActiveArc()
	end)
	rawset(self, "Hide", function(_)
		hidden = true
		updateActiveArc()
	end)

	-- Apply params
	for key, value in pairs(params) do self[key] = value end
	-- Just in case there were no params
	updateActiveArc()
	return self, protected
end


-- Default constructor
function Tactics.CircleProgress:New(params)
	-- Can not mix mutiple returns with call params
	local public, protected = Tactics.WidgetBase:New()
	return Tactics.CircleProgress:Bless(public, protected, params)
end


-- TESTING TESTING TESTING
local selfTest = false
if selfTest then
	Attunement.AddOnLoad(function()
		local hp = Tactics.CircleProgress:New{
			Layer='Dialog',
			radius=96,
			thickness=16,
			degrees=160,
			Color='#6600FF00',
			RotationDegrees=160/2,
			X=UI.PrimaryMonitorWidth/2,
			Y=UI.PrimaryMonitorHeight/2
		}
		local mp = Tactics.CircleProgress:New{
			Layer='Dialog',
			radius=88,
			thickness=8,
			degrees=160,
			Color='#660000FF',
			RotationDegrees=160/2,
			X=UI.PrimaryMonitorWidth/2,
			Y=UI.PrimaryMonitorHeight/2
		}
		-- This on is pie based
		local tp = Tactics.CircleProgress:New{
			Layer='Dialog',
			radius=0,
			thickness=88,
			degrees=160,
			Color='#66FF0000',
			RotationDegrees=160/2,
			X=UI.PrimaryMonitorWidth/2,
			Y=UI.PrimaryMonitorHeight/2
		}
		hp:Show()
		mp:Show()
		tp:Show()

		Attunement.Interval:New(1000, function(ticks)
			tp.Percent = math.random(0,100)
			hp.Percent = math.random(0,100)
			mp.Percent = math.random(0,100)
			tp.Scale = (math.random()+0.5)*2
			hp.Scale = tp.Scale
			mp.Scale = tp.Scale
			tp.RotationDegrees=math.random(0,360)
			hp.RotationDegrees=math.random(0,360)
			mp.RotationDegrees=math.random(0,360)
		end)
	end)
end
