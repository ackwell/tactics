Circle Segment
============================
![example picture](circle-segment-widget-1.png)

Draws a part of a circle. This could be an arc or a pie shaped wedge.

# Pie Wedge
To make a pie shaped wedge set the radius to 0 and use the thickness to control the radius of the circle.
```Lua
		local tp = Tactics.CircleSegment:New{
			Layer='Dialog',
			radius=0,
			thickness=108,
			startDegrees=200,
			endDegrees=100,
			Color='#55705656'
		}
```
# Arc
An Arc is kind of like a doughnut, there is a larger outer ring the size of which is controlled bu `radius`+`thickness` and a hollow inner ring controlled by `radius`.
```Lua
		local mp = Tactics.CircleSegment:New{
			Layer='Dialog',
			radius=108,
			thickness=8,
			startDegrees=0,
			endDegrees=270,
			Color='#550000FF'
		}
```

# Attributes
The circle segment object has several attributes, this may be set by supplying them in the form of a table at construction, or post construction using the `.` operator.
## X, Y
These control the X and Y location of the object, they are based on circle center.

## Color
Controls color of the object in the form `#01234567` ARGB.

## RotationDegrees
How much to rotation (spin) the object, default is 0.

## Scale
How much to increase or decrease the size of the object relative to its creation size, 1.0 would be orginal, 0.5 is 1/2 size, 2.0 double size.

## Hidden
Set to `false` to show the object, set to `true` to hide the object.

This is added so that it can be set at construction instead of having to call the `Show()` and `Hide()` functions.

# Functions
## Show()
Set the object to be displayed on the screen

## Hide()
Set the object to be not displayed on the screen.