Circle Progress
============================
![example picture](circle-progress-widget-1.png)

Uses mutiple CircleSegment's to construct a circular arc or pie based progress bar that is controlled by simply assigning 0-100 % to the Percent attribute.

Requires `circle-segment-widget.lua`

# Pie Wedge
To make a pie shaped wedge set the radius to 0 and use the thickness to control the radius of the circle.
```Lua
		local tp = Tactics.CircleProgress:New{
			Layer='Dialog',
			radius=0,
			thickness=88,
			degrees=160,
			Color='#66FF0000',
			RotationDegrees=160/2,
			X=UI.PrimaryMonitorWidth/2,
			Y=UI.PrimaryMonitorHeight/2
		}
```
# Arc
An Arc is kind of like a doughnut, there is a larger outer ring the size of which is controlled buy `radius`+`thickness` and a hollow inner ring controlled by `radius`.
```Lua
		local mp = Tactics.CircleProgress:New{
			Layer='Dialog',
			radius=88,
			thickness=8,
			degrees=160,
			Color='#660000FF',
			RotationDegrees=160/2,
			X=UI.PrimaryMonitorWidth/2,
			Y=UI.PrimaryMonitorHeight/2
		}
```

# Attributes
The circle progress object has several attributes, these may be set by supplying them in the form of a table at construction, or post construction using the `.` operator.
## X, Y
These control the X and Y location of the object, they are based on circle center.

## Color
Controls color of the object in the form `#01234567` ARGB.

## RotationDegrees
How much to rotation (spin) the object, default is 0.

## Scale
How much to increase or decrease the size of the object relative to its creation size, 1.0 would be orginal, 0.5 is 1/2 size, 2.0 double size.

## Percent
Number between 0 and 100 inclusive. This indicates the `percent` of the circle/arc/wedge that is showen. 0 would snow none of the object where 100 would show all `degrees` of the object.

To state another way, if `degrees` was set to 90 at construction, setting `Percent` to 100 will show all 90 degrees.

## Hidden
Set to `false` to show the object, set to `true` to hide the object.

This is added so that it can be set at construction instead of having to call the `Show()` and `Hide()` functions.

# Functions
## Show()
Set the object to be displayed on the screen

## Hide()
Set the object to be not displayed on the screen.