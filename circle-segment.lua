
-- In the widgets i'm going to depend on load
-- order, you must load widget-base.lua before this.
-- That's great and all, but let's be nice and at least try and fail gracefully.
if not Tactics then
	Tactics = {}
end

-- Constructs a segment of a circle.
-- This could be a pie shaped wedge
-- Or it could be a "thick line" along part of the circumfrance.
-- 4 args are curently required.
-- radius - The radius of the inner circle
-- thickness - How many pixes thick the line is.
-- -- if radius is 0, then thickness would become the radius of the
-- -- outer circle and a pie shaped wedge would get made.
-- -- if radius is >0 then your more or less getting a hollow circle
-- -- or part of one.
-- startDegrees - The angle in degree where the segment will start
-- endDegrees - The angle in degree where the segment will stop
-- The segmet is aways made off a cicle who's center is at 0,0
-- Thus, if what you draw is not in the 270-360 degree range
-- you will not see it on the screen till you shift the image
-- via the X and Y attributes into the visable area of the screen.
Tactics.CircleSegment = {}
function Tactics.CircleSegment:Bless(self, protected, params)
	-- Arg checking
	assert(params ~= nil, "Params are required")
	assert(params.radius ~= nil, "Must provide radius")
	assert(type(params.radius)=="number", "Radius must be a number")
	assert(params.radius >= 0, "Radius must be at least 0")
	assert(params.radius%1==0, "Radius must be a whole number")
	assert(params.thickness ~= nil, "Must provide thickness")
	assert(type(params.thickness) == "number", "Thickness must be a number")
	assert(params.thickness >= 1, "Thickness must be at least 1")
	assert(params.thickness%1==0, "Thickness must be a whole number")
	assert(params.startDegrees ~= nil, "Must provide startDegrees")
	assert(type(params.startDegrees) == "number", "startDegress must be a number")
	assert(params.endDegrees ~= nil, "Must provide startDegrees")
	assert(type(params.endDegrees) == "number", "startDegress must be a number")

	-- Private variables
	local thickness = params.thickness
	local radius = params.radius
	local cos = math.cos
	local sin = math.sin
	local rad = math.rad
	local deg = math.deg
	local startDegrees = params.startDegrees
	local endDegrees = params.endDegrees
	local layer = params.Layer or "Dialog"
	-- outer arc draws anti clockwise
	local outerArcStartX = (radius+thickness)*cos(rad(startDegrees))
	local outerArcStartY = (radius+thickness)*sin(rad(startDegrees))*-1
	local outerArcEndX = (radius+thickness)*cos(rad(endDegrees))
	local outerArcEndY = (radius+thickness)*sin(rad(endDegrees))*-1
	-- inner arc draws clockwise
	local innerArcStartX = (radius)*cos(rad(endDegrees))
	local innerArcStartY = (radius)*sin(rad(endDegrees))*-1
	local innerArcEndX = (radius)*cos(rad(startDegrees))
	local innerArcEndY = (radius)*sin(rad(startDegrees))*-1
	local diff = (endDegrees-startDegrees+360)%360
	local arcSize = Attunement.Geometry.ARC_180_OR_LESS
	if diff > 180 then
		arcSize = Attunement.Geometry.ARC_180_OR_GREATER
	end

	-- Remove the required params so they do
	-- not screw with applying the params later
	params.thickness = nil
	params.radius = nil
	params.startDegrees = nil
	params.endDegrees = nil
	params.Layer = nil

	-- Make the arc
	-- Always drawing with circle center at 0,0
	local geo = Attunement.Geometry:New{Layer=layer}
	-- Start figure on the outer arc start
	geo:BeginFigure(outerArcStartX, outerArcStartY,
		Attunement.Geometry.FIGURE_TYPE_FILLED
	)
	-- Draw the outer arc
	geo:AddArc(arcSize,
		outerArcEndX, outerArcEndY,
		0,radius+thickness,radius+thickness,
		Attunement.Geometry.ARC_SWEEP_COUNTER_CLOCKWISE
	)
	-- draw attachemtn line to inner arc
	geo:AddLine(innerArcStartX, innerArcStartY)
	-- draw inner arc
	geo:AddArc(arcSize,
		innerArcEndX, innerArcEndY,
		0,radius,radius,
		Attunement.Geometry.ARC_SWEEP_CLOCKWISE
	)
	-- draw the attachment line to the outer arc
	geo:AddLine(outerArcStartX, outerArcStartY)
	-- Finish off the geo.
	geo:EndFigure(Attunement.Geometry.END_TYPE_CLOSED)
	-- Set rotation point
	geo.RotationCenterX = 0
	geo.RotationCenterY = 0
	geo.ScaleX = 1 -- This is not a location its the X stretch
	geo.ScaleY = 1 -- This is not a location its the Y stretch

	-- Public interfaces
	protected:AddAttrWriteHook("X", function(v) geo.X = v end)
	protected:AddAttrReadHook("X", function() return geo.X end)
	protected:AddAttrWriteHook("Y", function(v) geo.Y = v end)
	protected:AddAttrReadHook("Y", function() return geo.Y end)
	protected:AddAttrWriteHook("Color", function(v) geo.Color = v end)
	protected:AddAttrReadHook("Color", function() return geo.Color end)
	protected:AddAttrWriteHook("Hidden", function(v) geo.Hidden = v end)
	protected:AddAttrReadHook("Hidden", function() return geo.Hidden end)
	protected:AddAttrWriteHook("RotationDegrees", function(v)
		geo.Rotation=rad(v)
	end)
	protected:AddAttrReadHook("RotationDegrees",function()
		return deg(geo.Rotation)
	end)
	protected:AddAttrWriteHook("Scale",function(v)
		geo.ScaleX=v
		geo.ScaleY=v
	end)
	protected:AddAttrReadHook("Scale",function()
		return geo.ScaleX
	end)
	rawset(self, "Show", function(_) geo:Show() end)
	rawset(self, "Hide", function(_) geo:Hide() end)

	-- Apply params
	for key, value in pairs(params) do self[key] = value end
	return self, protected
end

-- Default constructor
function Tactics.CircleSegment:New(params)
	-- Can not mix mutiple returns with call params
	local public, protected = Tactics.WidgetBase:New()
	return Tactics.CircleSegment:Bless(public, protected, params)
end


-- TESTING TESTING TESTING
local selfTest = false
if selfTest then
	Attunement.Debug.Console:Enable()
	print('YES HELLO WORLD GO FUCK YOURSELF')
	Attunement.AddOnLoad(function()
		-- Make a afew different segments
		-- We'll model them after a TP/MP/HP idea
		-- TP will be a pie wedge
		-- HP and MP are arcs
		local tp = Tactics.CircleSegment:New{
			Layer='Dialog',
			radius=0,
			thickness=108,
			startDegrees=200,
			endDegrees=100,
			Color='#55705656'
		}
		local mp = Tactics.CircleSegment:New{
			Layer='Dialog',
			radius=108,
			thickness=8,
			startDegrees=0,
			endDegrees=270,
			Color='#550000FF'
		}
		local hp = Tactics.CircleSegment:New{
			Layer='Dialog',
			radius=116,
			thickness=16,
			startDegrees=270,
			endDegrees=90,
			Color='#5500FF00'
		}
		tp.X=UI.PrimaryMonitorWidth/2
		tp.Y=UI.PrimaryMonitorHeight/2
		tp.RotationDegrees = 0
		tp:Show()
		mp.X=UI.PrimaryMonitorWidth/2
		mp.Y=UI.PrimaryMonitorHeight/2
		tp.RotationDegrees = 0
		mp:Show()
		hp.X=UI.PrimaryMonitorWidth/2
		hp.Y=UI.PrimaryMonitorHeight/2
		tp.RotationDegrees = 0
		hp:Show()

		Attunement.AddOnFrame(function(ticks)
			-- Spin and scale the circle segments
			hp.RotationDegrees = hp.RotationDegrees+1
			tp.RotationDegrees = tp.RotationDegrees+1
			mp.RotationDegrees = mp.RotationDegrees-1
			hp.Scale = (hp.Scale + 0.02) % 3.0
			mp.Scale = (mp.Scale + 0.02) % 3.0
			tp.Scale = (tp.Scale + 0.02) % 3.0
		end)
	end)
end
